Para esse código funcionar deve-se ter instalado o MySQL e um servidor php como Wamp.

No MySQL deve-se digitar os seguintes códigos:

create database 'teste';
create table orders(
	id_order int(11),
	start_delivery varchar(50),
	end_delivery varchar(50),
	name varchar(50),
	last_name varchar(50),
	street_name varchar(50),
	zone varchar(50),
	street_number int(11),
	postal_code int(11),
	city varchar(50),
	state varchar(2),
	shopper varchar(50),
	PRIMARY KEY(id_order)
);
create table order_items(
	order_id int(11),
	product_id int(11),
	product_name text,
	price decimal(10,2),
	quantity int(11),
	barcode varchar(20),
	PRIMARY KEY(order_id, product_id)
);

Carregar os dados do arquivo excel na base.
Fazer o download do código e o testar.  
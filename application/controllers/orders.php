<?php if(! defined('BASEPATH')) exit ('No direct script access allowed');

class orders extends CI_Controller{
	public function index($inicio=0){

		$orderId='';
		$orderBy='';
		if($this->input->post("orderId")!='' and $this->input->post("orderBy")!=''){
			$orderId = $this->input->post("orderId");
			$orderBy = $this->input->post("orderBy");
		}

		$this->load->model("orders_model");

		$orders = $this->orders_model->getAll(0,0,$orderId,$orderBy);

		//Pagination
		$this->load->library('pagination');
		$config['base_url'] = '/codeigniter3/index.php/orders/index/';
		$config['total_rows'] = count($orders);
		$config['per_page'] = 9;
		$config['num_links'] = 5;

		$qtde = $config['per_page'];
		$this->pagination->initialize($config);	

		$orders = $this->orders_model->getAll($qtde, $inicio,$orderId,$orderBy);

		$searchItems = array(
			0 => array(
				'text'=>'Start Delivery',
				'value'=>'start_delivery'
			),
			1 => array(
				'text'=>'End Delivery',
				'value'=>'end_delivery'
			),
			2 => array(
				'text'=>'Name',
				'value'=>'name'
			),
			4 => array(
				'text'=>'Order`s Number',
				'value'=>'id_order'
			)
		);
		$dados = array(
			'pagination' => $this->pagination->create_links(),
			'searchItems'=>$searchItems,
			'orders'=>$orders
		);

		$this->load->view("orders/orders.php",$dados);
	}

	public function ajaxAddress($name){		
		$this->load->model("orders_model");
		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			$order = $this->orders_model->getAddressByName($name);

			echo json_encode( array( 'order' => $order ) );
		}
	}

	public function pdf(){

		$this->load->model("orders_model");

		$orders = $this->orders_model->getAll(0,0);
		$dados = array(
			'orders' => $orders
		);

		//Carregando a biblioteca mPDF
		$this->load->library('mpdf');

		//Inicia o buffer, qualquer HTML que for sair agora sera capturado para o buffer
		ob_start();
		?>
		<?php
		//Fazendo o include de um HTML em outro arquivo, ficara retido no buffer
		$this->load->view("orders/pdf.php",$dados);
		 
		//Limpa o buffer jogando todo o HTML em uma variavel.
		$html = ob_get_clean();
		$mpdf=new mPDF('utf-8', 'A4-L');
		$mpdf->WriteHTML($html);
		//Colocando o rodape
		$mpdf->SetFooter('{DATE j/m/Y H:i}|Página teste');
		$mpdf->Output();
		exit;
	}

	public function excel(){

		$this->load->library("PHPExcel");
    	$planilha = $this->phpexcel;

    	$planilha->setActiveSheetIndex(0)->setCellValue('A1','id_order');
    	$planilha->setActiveSheetIndex(0)->setCellValue('B1','start_delivery');
    	$planilha->setActiveSheetIndex(0)->setCellValue('C1','end_delivery');
    	$planilha->setActiveSheetIndex(0)->setCellValue('D1','name');
    	$planilha->setActiveSheetIndex(0)->setCellValue('E1','last_name');
    	$planilha->setActiveSheetIndex(0)->setCellValue('F1','street_name');
    	$planilha->setActiveSheetIndex(0)->setCellValue('G1','zone');
    	$planilha->setActiveSheetIndex(0)->setCellValue('H1','street_number');
    	$planilha->setActiveSheetIndex(0)->setCellValue('I1','postal_code');
    	$planilha->setActiveSheetIndex(0)->setCellValue('J1','city');
    	$planilha->setActiveSheetIndex(0)->setCellValue('K1','state');
    	$planilha->setActiveSheetIndex(0)->setCellValue('L1','shopper_name');

    	$this->load->model("orders_model");

		$orders = $this->orders_model->getAll(0,0);

		$contador = 2;

		foreach($orders as $order){
			$planilha->setActiveSheetIndex(0)->setCellValue('A'.$contador,$order["id_order"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('B'.$contador,$order["start_delivery"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('C'.$contador,$order["end_delivery"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('D'.$contador,$order["name"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('E'.$contador,$order["last_name"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('F'.$contador,$order["street_name"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('G'.$contador,$order["zone"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('H'.$contador,$order["street_number"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('I'.$contador,$order["postal_code"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('J'.$contador,$order["city"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('K'.$contador,$order["state"]);
	    	$planilha->setActiveSheetIndex(0)->setCellValue('L'.$contador,$order["shopper_name"]);
	    	
			$contador++;
		}
		$planilha->getActiveSheet()->setTitle("Orders");

    	$objGravar = PHPExcel_IOFactory::createWriter($planilha, "Excel2007");

    	// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="Orders.xlsx"');

		// Write file to the browser
		$objGravar->save('php://output');
	}
}
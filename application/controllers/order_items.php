<?php if(! defined('BASEPATH')) exit ('No direct script access allowed');

class order_items extends CI_Controller{
	public function index($id_order){

		$this->load->model("order_items_model");

		$orderItems = $this->order_items_model->getAll($id_order);

		$dados = array(
			"order_items"=>$orderItems,
			"id_order"=>$id_order
		);

		$this->load->view("orders/order_items.php",$dados);
	}
}
<?php
class Orders_model extends CI_Model{

	public function getAll($qtde = 0, $inicio = 0, $orderId='', $orderBy=''){
		if($qtde>0) $this->db->limit($qtde,$inicio);		
		if($orderId != '') 		
			$this->db->where($orderBy,$orderId);	
		$this->db->order_by("id_order");	
		$this->db->join('clients', 'orders.name = clients.id');	
		$this->db->from("orders");		
		return $this->db->get()->result_array();
	}

	public function getAddressByName($name){
		$this->db->select("name,street_name,zone,street_number,postal_code,city,state");		
		$this->db->where("id",$name);		
		$this->db->from("clients");		
		return $this->db->get()->row_array();
	}
}
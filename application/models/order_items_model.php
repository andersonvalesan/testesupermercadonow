<?php
class Order_Items_model extends CI_Model{
	
	public function getAll($id_order){
		$this->db->from("order_items");
		$this->db->order_by("order_id");	
		$this->db->where("order_id",$id_order);
		$this->db->join('product', 'order_items.product_id = product.product_id');	
		return $this->db->get()->result_array();
	}
}
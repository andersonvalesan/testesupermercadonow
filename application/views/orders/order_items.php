<!DOCTYPE html>
<html>
<head>
	<title>Order Items</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url("css/bootstrap.css") ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url("css/test.css") ?>">
</head>
<body>
	<?php include("/../header.php");?>
	<h2 class="title" style="margin-top: 0%;">Order Items From Order: <?=$id_order;?></h2>
	<a href="/codeigniter3/index.php/orders" class='btn btn-primary'>Orders</a>
	<table class='table'>
		<thead>
			<tr style="font-weight: bold;">
				<td scope="col">Id Product</td>
				<td scope="col">Product Name</td>
				<td scope="col">Price</td>
				<td scope="col">Quantity</td>
				<td scope="col">Total</td>
			</tr>
		</thead>
	<?php 
	$totalItems = 0;
	$totalPrice = 0;
	$striped = 0;
	foreach ($order_items as $item) :
		$totalItems+=$item['quantity'];
		$totalPrice+=$item['price']*$item['quantity'];
		include("/../striped.php");
	?>
		<tbody>
			<tr style="background-color: <?= $backgroundColor;?>;">
				<td><?php echo $item['product_id']; ?></td>
				<td><?php echo $item['product_name']; ?></td>
				<td><?php echo "R$ ".number_format($item['price'],2,',','.'); ?></td>
				<td><?php echo $item['quantity']; ?></td>
				<td><?php echo "R$ ".number_format($item['price']*$item['quantity'],2,',','.'); ?></td>
			</tr>
		</tbody>
	<?php 
	$striped++;
	endforeach;?>
	<tr style="font-weight: bold;">
		<td colspan="3" style="text-align: center;">Total:</td>
		<td><?= $totalItems; ?></td>
		<td><?= "R$ ".number_format($totalPrice,2,',','.'); ?></td>
	</tr>
</body>
</html>
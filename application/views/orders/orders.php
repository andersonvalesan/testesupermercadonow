<!DOCTYPE html>
<html>
<head>
	<title>Orders</title>	
	<link rel="stylesheet" type="text/css" href="<?= base_url("css/bootstrap.css") ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url("css/test.css") ?>">	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>	
</head>
<body>
	<?php include("/../header.php");?>
	<h2 class="title" style="margin-top: 0%;">List of Orders</h2>	
	<div class="list-group">
	<?php
		echo form_open("/orders");
		echo form_label("Search Order ","orderId",array("style" => "float:left;"));
		echo form_input(array(
			"name" => "orderId",
			"id" => "orderId",
			"style" => "float:left;height:30px;",
			"class" => "list-group-item",
		));
	?>
	</div>
<div class="list-group" style="float: left;margin-top: -1%;">
	<label>by:</label>
	<td>
		<select name="orderBy" id="orderBy" class="form-control" style="float: left;height:30px;margin-top: -15%;margin-left: 15%;">
		
		<?php foreach ($searchItems as $item):
			$selecao = $item['text']?"selected='selected'":"";
		?>
			<option value="<?= $item['value'];?>" <?=$selecao;?>>
				<?= $item['text'];?>
			</option>
		<?php 
		endforeach;
		?>
		</select>
	</td>
</div>
<?php

		echo form_submit(array(
			"value" => "Search",
			"style" => "float:left; padding:0px 2px; margin-left:3%;margin-top:-1%;",
			"class" => "btn btn-primary",
		));

		echo form_close();
	?>
	</div>
	<div style="float: left; margin-left: 5%; margin-top: -1%;">
		<a class="btn btn-danger" href=<?="/codeigniter3/index.php/orders/pdf/";?> style="padding:0px 2px;">PDF</a>
		<a class="btn btn-success" href=<?="/codeigniter3/index.php/orders/excel/";?> style="padding:0px 2px;">Excel</a>
	</div>
	<table class='table' style="overflow-x: auto;min-width: 800px;">
		<thead>
			<tr style="font-weight: bold;">
				<td scope="col">Actions</td>
				<td scope="col">Order's Number</td>
				<td scope="col">Start Delivery</td>
				<td scope="col">End Delivery</td>
				<td scope="col">Name</td>
				<td scope="col">Last Name</td>
				<td scope="col">Shopper's Name</td>
			</tr>
		</thead>
		<tbody>
	<?php 
	$striped=0;
	foreach ($orders as $order):
		include("/../striped.php");?>
		<tr style="background-color: <?= $backgroundColor;?>;">
			<td>
				<a href=<?= "/codeigniter3/index.php/order_items/index/".$order['id_order'];?> class='btn btn-primary'>Items</a>
				<button class="btn btn-primary button" value="<?= $order['id'];?>">Address</button>
			</td>
			<td><?php echo $order['id_order']; ?></td>
			<td><?php echo $order['start_delivery']; ?></td>
			<td><?php echo $order['end_delivery']; ?></td>
			<td><?php echo $order['name']; ?></td>
			<td><?php echo $order['last_name']; ?></td>
			<td><?php echo $order['shopper_name']; ?></td>
		</tr>

	<tr id=<?='address'.$order['id'];?>></tr>
	<?php 
	$striped++;
	endforeach;?>
		</tbody>
	</table>
<button class = "btn btn-primary" id="close">Hide</button><br/>
<?php
if(isset($pagination)):
?>
	<div style="font-size:18px;color:black!important;">
		<?php echo($pagination);?>
	</div>
<?php
endif;
?>
<script type="text/javascript">
    $( document ).ready(function() {
        $('.button').click(
        	function(){
	            $(this).attr('id','close');
        		var value = $(this).attr('value');
        		console.log(value);
                $.ajax({
                    'url' : "<?php echo '/codeigniter3/index.php/orders/ajaxAddress/'?>"+value,
                    'type' : 'GET',
                    'success' : function(data){ 
                        var order = JSON.parse( data );  
                        console.log(order);                      	                        
	                    $('#address'+value).html( 
	                    	'<div class="address"><br/><p><b>Name:</b> '+ order.order.name +'</p>'+
	                    	'<p><b>Street Name:</b> '+ order.order.street_name +'</p>'+
	                    	'<p><b>Street Number:</b> '+ order.order.street_number +'</p>'+
	                    	'<p><b>Zone:</b> '+ order.order.zone +'</p>'+
	                    	'<p><b>City:</b> '+ order.order.city +'</p>'+
	                    	'<p><b>State:</b> '+ order.order.state +'</p>'+
	                    	'<p><b>Postal Code:</b> '+ order.order.postal_code +'</p>'+
	                    	'</div>'
	                    );
	                }
                });
            });              
        ;
    });


    $("#close").click(function(){
        //console.log('teste');
        $(".address").hide();
    });
</script>	
</body>
</html>
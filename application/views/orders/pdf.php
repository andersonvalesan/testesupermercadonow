<h1 style="text-align: center;">Orders</h1>

<table class='table' style="border:1px solid black;width: 100%;border-collapse: collapse;">
		<thead>
			<tr style="font-weight: bold; border:1px solid black;">
				<td scope="col">Order's Number</td>
				<td scope="col">Start Delivery</td>
				<td scope="col">End Delivery</td>
				<td scope="col">Name</td>
				<td scope="col">Last Name</td>
			</tr>
		</thead>
		<tbody>
	<?php 
	foreach ($orders as $order):?>
		<tr>
			<td style="border:1px solid black;"><?php echo $order['id_order']; ?></td>
			<td style="border:1px solid black;"><?php echo $order['start_delivery']; ?></td>
			<td style="border:1px solid black;"><?php echo $order['end_delivery']; ?></td>
			<td style="border:1px solid black;"><?php echo $order['name']; ?></td>
			<td style="border:1px solid black;"><?php echo $order['last_name']; ?></td>
		</tr>
	<?php 
	endforeach;?>
		</tbody>
	</table>